const dotenv = require('dotenv');
const app = require('./app');
const mongoose  = require('mongoose')

dotenv.config({ path: './config.env' });
const DB = process.env.DATABASE.replace("<PASSWORD>",process.env.DATABASE_PASSWORD);

mongoose.connect(DB, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify:false
}).then(()=>{console.log("DB connection successful!");});

const tourSchema = new mongoose.Schema({
  name:{
    type:String,
    required:[true,'A tour must have a name'],
    unique:true
  },
  rating:{
    type:Number,
    default:4.5
  },
  price:{
    type:Number,
    require:[true,'A tour must have a name']
  }
});
const Tour = mongoose.model('Tour',tourSchema)

const testTour = new Tour({
  name:"The Forest Hiker",
  rating:7.7,
  price:69
})

const testTour1 = new Tour({
  name:"The Paris Robber",
  rating:8.0,
  price:700
})

testTour.save().then(doc=>console.log(doc)).catch(err=>console.log('ERROR',err));
testTour1.save().then(doc=>console.log(doc)).catch(err=>console.log('ERROR',err));

const port = process.env.PORT || 3000;
app.listen(port, () => {
  console.log(`App running on port ${port}...`);
});
